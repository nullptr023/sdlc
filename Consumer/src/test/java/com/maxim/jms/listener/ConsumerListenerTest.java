package test.java.com.maxim.jms.listener;

import javax.jms.JMSException;
import javax.jms.TextMessage;
import main.java.com.maxim.jms.listener.ConsumerListener;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import static org.easymock.EasyMock.verify;
import static org.easymock.EasyMock.createMock;
import static org.easymock.EasyMock.replay;
import static org.easymock.EasyMock.expect;

public class ConsumerListenerTest {
	
	private TextMessage message;
	
	private ApplicationContext context;
	private ConsumerListener listener;
	private String json = "{vendorName:\"Microsofttest3\",firstName:\"BobTest3\",lastName:\"SmithTest\",address:\"123 Main test3\",city:\"TulsaTest3\",state:\"OKTest\",zip:\"71345Test3\",email:\"Bob@microsoft.test3\",phoneNumber:\"test-123-3333\"}";

	@Before
	public void setUp() throws Exception {
		context = new ClassPathXmlApplicationContext("/spring/application-config.xml");
		listener = (ConsumerListener) context.getBean("consumerListener");
		// mock is used for testing 
		message = createMock(TextMessage.class);
	}

	@After
	public void tearDown() throws Exception {
		// close the context
		((ConfigurableApplicationContext)context).close();
	}

	@Test
	public void testOnMessage() throws JMSException {
		// expected message is from message.getText and returns a json
		expect(message.getText()).andReturn(json);
		// to run it c
		replay(message);
		listener.onMessage(message);
		verify(message);
	}

}
