package main.java.com.maxim.jms.listener;

import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.TextMessage;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.stereotype.Component;

import main.java.com.maxim.jms.adapter.ConsumerAdapter;

// This class is connected to ActiveMQ Server via mvx-config bean:id: JMSMessageListenerContainer
// This class is the message listener for the JMS
@Component
public class ConsumerListener implements MessageListener {

	private static Logger logger = LogManager.getLogger(ConsumerListener.class.getName());
	
	// connects with ActiveMQ Server
	@Autowired
	private JmsTemplate jmsTemplate;
	
	@Autowired
	ConsumerAdapter consumerAdapter;
	
	@Override
	public void onMessage(Message message) {
		String json = null;
		logger.info("In onMessage");
		if (message instanceof TextMessage) {
			try {
				json = ((TextMessage) message).getText();
				logger.info("Sending JSON to DB: "+ json);
				consumerAdapter.sendToMongo(json);
			} catch (JMSException e) {
				// if there is an error, write in on RME Queue (error queues)
				logger.error("Message: "+ json);
				jmsTemplate.convertAndSend(json);
			} catch (Exception e) {
				logger.error("Message: "+ json);
				jmsTemplate.convertAndSend(json);
			}
		}
	}

}
